#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/types.h>
#include <curl/curl.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>
#include <string.h>
#include <fcntl.h>


// array index 0 -> quote
// array index 1 -> music
pthread_t tid[100]; //inisialisasi array untuk menampung thread id
pid_t child_id;
int status;

// diperlukan untuk menampung multiple argument utk dikirim ke func saat pthread_create
typedef struct arg
{
    char str1[100];
    char str2[100];
}arg_t;


void download(char* link)
{
    printf("ini download");
    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
    if (child_id == 0) 
    {
        printf("child PID = %d\n", getpid());
        char *argv[] = {"/usr/bin/wget", "-nd", link, NULL};
        execv("/usr/bin/wget", argv);
    }           
    while ((wait(&status)) > 0);
}


void* ekstrak(void* multi_arg)
{
    printf("EKSTRAK\n\n\n");
    arg_t *arg = (arg_t *) multi_arg;
    char* nama_file = (char *) arg->str1;
    char* dir = (char *) arg->str2;

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0) 
    {
        char *argv[] = {"/bin/unzip", nama_file, "-d", dir, NULL};
        execv("/bin/unzip", argv);
    } 
    while ((wait(&status)) > 0);
}

void* decode(void* multi_arg)
{
    arg_t *arg = (arg_t *) multi_arg;
    char* file = (char *) arg->str1;
    char* file_hasil = (char *) arg->str2;

    FILE* fptr;

    fptr = fopen(file_hasil, "w");
    fclose(fptr);

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0) 
    {
        int fd = open(file_hasil, O_WRONLY | O_CREAT, 0777);
        if(file == -1) return 2; // gatau kenapa gini. ngikut di yt

        int fd = dup2(file, STDOUT_FILENO); //biasanya kita standart output itu ke terminal. dengan ini, kita mengganti setelan tempat outputnya jadi ke "file".
        
        char *argv[] = {"/usr/bin/base64", file, NULL};
        execv("usr/bin/base64", argv);
    } 
    while ((wait(&status)) > 0);
}




int main()
{
    int i;
    int i_tid = 0; // i untuk tid
    char link_quote[] = "https://drive.google.com/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download";
    char link_music[] = "https://drive.google.com/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download";

    // array index 0 -> quote
    // array index 1 -> music
    char file[3][200];

    strcpy(file[0], "uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download"); //file quote
    strcpy(file[1], "uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download"); //file music
    char nama_folder[3][30];
    strcpy(nama_folder[0], "quote");
    strcpy(nama_folder[1], "music");
    
    download(link_quote); //download dari link pada link_quote
    download(link_music);

    // melakukan multithread untuk melakukan ekstrak / unzip file quote dan music berbarengan

    arg_t* arg_ekstrak[3]; // argumen untuk fungsi ekstrak (bertipe struct)

    for(i=0; i<2; i++)
    {
        arg_ekstrak[i] = (arg_t *) malloc(sizeof(arg_t));

        strcpy(arg_ekstrak[i]->str1, file[i_tid]); // nama file yg ingin di-unzip
        strcpy(arg_ekstrak[i]->str2, nama_folder[i_tid]); //nama dir baru

        int err = pthread_create(&tid[i_tid], NULL, ekstrak, (void *) arg_ekstrak[i]);

        if(err!=0) //cek error
            printf("\n can't create thread : [%s]",strerror(err));
        else 
            printf("\n create thread success\n");
        
        i_tid++;
    }


    // catatan :
    // file di folder music ada 9. file di folder quote ada 9.
    // total file : 18
    int n_file = 18;

    char file_decode[20][20] = {"q1.txt", "q2.txt", "q3.txt", "q4.txt", "q5.txt", "q6.txt", "q7.txt", "q8.txt", "q9.txt", "m1.txt", "m2.txt", "m3.txt", "m4.txt", "m5.txt", "m6.txt", "m7.txt", "m8.txt", "m9.txt"};
    arg_t* arg_decode[3]; // argumen untuk fungsi decode (bertipe struct)

    for(i=0; i<n_file; i++)
    {
        arg_decode[i] = (arg_t *) malloc(sizeof(arg_t));
        strcpy(arg_decode[i]->str1, file_decode[i]); // nama file yg ingin di-unzip

        //nama file baru
        if(i>9)
            strcpy(arg_decode[i]->str2, "quote"); 
        else
            strcpy(arg_decode[i]->str2, "music"); 

        int err = pthread_create(&tid[i_tid], NULL, decode, (void *) arg_decode[i]);

        if(err!=0) //cek error
            printf("\n can't create thread : [%s]",strerror(err));
        else 
            printf("\n create thread success\n");

        i_tid++;
    }

    pthread_exit(NULL);
 
}
