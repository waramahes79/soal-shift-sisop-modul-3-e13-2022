#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <wait.h>
#include <stdlib.h>
#include <unistd.h>
#include <pwd.h>
#include <grp.h>
#include <string.h>
#include <sys/stat.h>
#include <pthread.h>
#include <ctype.h>
#define deb(s) printf("%s\n",s)

pthread_t tid1[100],tid2;
int k=0;

void* fungsiexec(void* arg){
    FILE *read_ptr;
    char buffer[BUFSIZ+1];
    char* command = (char *) arg;
    int chars_read;

    memset(buffer,'\0',sizeof(buffer));

    read_ptr = popen(command,"r");

    if(read_ptr != NULL){
        chars_read = fread(buffer, sizeof(char),BUFSIZ,read_ptr);
        if(chars_read > 0) printf("%s\n",buffer);
        pclose(read_ptr);
    }
}

void emover(char* dir){
    DIR *dp;
    struct dirent *ep;
    char command[100];
    dp = opendir(dir);
    
    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            if(strcmp(".",ep->d_name)==0) continue;
            if(strcmp("..",ep->d_name)==0) continue;
            // int len = strlen(ep->d_name);
            // char result[len+1];
            // for(int i=0;i<len;i++){
            //     result[i]=tolower(ep->d_name[i]);
            // }
            //printf("%s ",ep->d_name);
            //directory
            char command[150]="mv ";
             strcat(command,dir); strcat(command,"/");strcat(command,ep->d_name);strcat(command," hartakarun");
            pthread_create(&(tid1[k]), NULL, fungsiexec,(void *) command); k++;   
            pthread_join(tid1[k-1],NULL);
        }
        (void) closedir(dp);
    } else perror ("Couldn't open the directory");
}

void efolder(char* dir){
    DIR *dp;
    struct dirent *ep;
    char command[100];
    dp = opendir(dir);
    
    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            if(strcmp(".",ep->d_name)==0) continue;
            if(strcmp("..",ep->d_name)==0) continue;
            // int len = strlen(ep->d_name);
            // char result[len+1];
            // for(int i=0;i<len;i++){
            //     result[i]=tolower(ep->d_name[i]);
            // }
            //printf("%s ",ep->d_name);
            //directory
            if(ep->d_type == DT_DIR){
                char dirr[150]=""; strcpy(dirr,dir);
                 strcat(dirr,"/"); strcat(dirr,ep->d_name); 
                 emover(dirr);

                char command[150]="rm -r \'";
                strcat(command,dir); strcat(command,"/"); strcat(command,ep->d_name); strcat(command,"\'");
                //printf("%s\n",command);
                pthread_create(&(tid1[k]), NULL, fungsiexec,(void *) command); k++;   
                pthread_join(tid1[k-1],NULL);
            }
        }
        (void) closedir(dp);
    } else perror ("Couldn't open the directory");
}

void grouper(char* dir){

    DIR *dp;
    struct dirent *ep;
    char command[100];
    dp = opendir(dir);
    
    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            if(strcmp(".",ep->d_name)==0) continue;
            if(strcmp("..",ep->d_name)==0) continue;
            // int len = strlen(ep->d_name);
            // char result[len+1];
            // for(int i=0;i<len;i++){
            //     result[i]=tolower(ep->d_name[i]);
            // }
            //printf("%s ",ep->d_name);
            //hidden
            if(ep->d_type == DT_DIR) continue;
            if(ep->d_name[0]=='.'){
                //deb("hidden");
                char command[150]="mv hartakarun/";
                strcat(command,ep->d_name); strcat(command," hartakarun/"); strcat(command,"hidden");
                pthread_create(&(tid1[k]), NULL, fungsiexec,(void *) command); k++;   
                pthread_join(tid1[k-1],NULL); 
            }
            // if(ep->d_name[0]=='.'){
            //     deb("hidden");
            //     char command[150]="mv \'hartakarun/";
            //     strcat(command,ep->d_name); strcat(command,"\' \'hartakarun/"); strcat(command,"hidden\'");
            //     pthread_create(&(tid1[k]), NULL, fungsiexec,(void *) command); k++;   
            //     pthread_join(tid1[k-1],NULL); 
            // }
            //unknown
            else if(strchr(ep->d_name,'.') == NULL){
                //deb("unknown");
                strcpy(command,"mv \'hartakarun/");
                strcat(command,ep->d_name); strcat(command,"\' hartakarun/"); strcat(command,"unknown"); 
                pthread_create(&(tid1[k]), NULL, fungsiexec,(void *) command); k++;
                pthread_join(tid1[k-1],NULL);
            }
            //regular
            else {
                //deb("reg");
                char nama[128];
                strcpy(nama,ep->d_name);
                //printf("%s\n",nama);
                char* ext = strchr(ep->d_name,'.'); ext++;
                while(strchr(ext,' ') != NULL){
                    strcpy(ext,strchr(ext,'.')); ext++;
                    //printf("%s\n",(ext));
                }
                //printf("%s\n",command);
                //printf("%s\n",(ext));
                int len = strlen(ext);
                char dest[len+1];
                for(int i =0;i<len;i++){
                    dest[i]= tolower(ext[i]);
                }
                strcpy(command,"mkdir -p hartakarun/");
                strcat(command,dest);
                pthread_create(&(tid1[k]), NULL, fungsiexec,(void *) command);k++;
                pthread_join(tid1[k-1],NULL);
                char command[256]="mv \'hartakarun/";
                strcat(command,nama); strcat(command,"\' \'hartakarun/");strcat(command,dest); strcat(command,"\'");
                //printf("%s\n",command);
                pthread_create(&(tid1[k]), NULL, fungsiexec,(void *) command); k++;
                pthread_join(tid1[k-1],NULL);
            }
        }
        (void) closedir (dp);
    } else perror ("Couldn't open the directory");
}

int main(){
    pthread_create(&(tid2), NULL, fungsiexec,(void *) "unzip -u hartakarun.zip"); 
    pthread_join(tid2,NULL);
    pthread_create(&(tid1[k]), NULL, fungsiexec,(void *) "chmod a+w .."); k++;
    efolder("hartakarun");
    pthread_create(&(tid1[k]), NULL, fungsiexec,(void *) "mkdir -p hartakarun/hidden"); k++;
    pthread_join(tid1[k-1],NULL);
    pthread_create(&(tid1[k]), NULL, fungsiexec,(void *) "mkdir -p hartakarun/unknown"); k++;
    pthread_join(tid1[k-1],NULL);
    grouper("hartakarun");

    if(chdir("/hartakarun") < 0) exit(EXIT_FAILURE);
    

    return 0;
}
